package hospital;

import hospital.exception.RemoveException;

public class HospitalClient {
    //This is public to make tests work
    public Hospital hospital;

    /**
     * @param hospital
     * constructor for hospitalClient
     */
    public HospitalClient(Hospital hospital) {
        this.hospital = hospital;
    }


    /**
     * @param args
     */
    public static void main(String[] args) {

    }
}