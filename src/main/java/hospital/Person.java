package hospital;

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     * Constructor for person
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        //checks that a persons values are not empty
        if (!firstName.isEmpty() && !lastName.isEmpty() && !socialSecurityNumber.isEmpty()){
        this.firstName = firstName;
        this.lastName = lastName;
        this.socialSecurityNumber = socialSecurityNumber;}
        //throws an exception if any of the values are empty
        else {
            throw new IllegalArgumentException("Persons first name, last name or security number can not be empty");
        }
    }

    /**
     * @return
     * getter for persons firstname
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName
     * sets persons first name
     */
    public void setFirstName(String firstName) {
        if (!firstName.isEmpty()){
        this.firstName = firstName;}
        else throw new IllegalArgumentException("First name can not be empty");
    }

    /**
     * @return
     * getter for persons last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName
     * sets persons last name
     */
    public void setLastName(String lastName) {
        if (!lastName.isEmpty()){
            this.lastName = lastName;}
        else throw new IllegalArgumentException("Last name can not be empty");
    }

    /**
     * @return
     * getter for persons social security number
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    /**
     * @param socialSecurityNumber
     * sets persons social security number
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        if (!socialSecurityNumber.isEmpty()){
        this.socialSecurityNumber = socialSecurityNumber;}
        else throw new IllegalArgumentException("Security number can not be empty");
    }

    /**
     * prints out persons full name
     */
    public void getFullName(){
        System.out.println(firstName +" " + lastName);
    }

    /**
     * @return
     * prints out person details
     */
    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }
}