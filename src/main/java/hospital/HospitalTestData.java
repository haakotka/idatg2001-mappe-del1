package hospital;
import hospital.healthpersonal.Nurse;
import hospital.healthpersonal.doctor.GeneralPractitioner;
import hospital.healthpersonal.doctor.Surgeon;
public final class HospitalTestData {
    private HospitalTestData() {
        // not called
    }
    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital){
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", "0"));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", "1"));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", "2"));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", "3"));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", "4"));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", "5"));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", "6"));
        emergency.getPatients().add(new Patient("Inga", "Lykke", "7"));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", "8"));
        hospital.addDepartment(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", "9"));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", "10"));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", "11"));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", "12"));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", "13"));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", "14"));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", "15"));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", "16"));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", "17"));
        hospital.addDepartment(childrenPolyclinic);
    }
}