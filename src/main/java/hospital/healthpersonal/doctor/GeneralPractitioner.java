package hospital.healthpersonal.doctor;

import hospital.Patient;

public class GeneralPractitioner extends Doctor {
    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     * constructor for GeneralPractitioner
     */
    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @param patient
     * @param diagnosis
     * sets diagnosis on patient. Method inherited from Doctor class
     */
    @Override
    public void setDiagnose(Patient patient, String diagnosis) {

    }
}
