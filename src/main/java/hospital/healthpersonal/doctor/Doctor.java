package hospital.healthpersonal.doctor;

import hospital.Employee;
import hospital.Patient;

public abstract class Doctor extends Employee {
    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     * constructor for doctor
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @param patient
     * @param diagnosis
     * method to set diagnosis on patient
     */
    public abstract void setDiagnose(Patient patient,String diagnosis);
}
