package hospital.healthpersonal.doctor;

import hospital.Patient;


public class Surgeon extends Doctor {
    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     * constructor for surgeon
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @param patient
     * @param diagnosis
     * sets diagnosis on patient. Method inherited from doctor class.
     */
    @Override
    public void setDiagnose(Patient patient, String diagnosis) {

    }
}

