package hospital.healthpersonal;

import hospital.Employee;

public class Nurse extends Employee {
    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     * constructor for nurse
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @return
     * prints out details about nurse. Method inherited from Person class
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
