package hospital.exception;

public class RemoveException extends Exception{
    /**
     * @param message
     */
    public RemoveException(String message) {
        super(message);
    }
}
