package hospital;

public class Patient extends Person implements Diagnosable{
    private String diagnosis="";


    /**
     * @return
     * getter for diagnosis
     */
    protected String getDiagnosis() {
        return diagnosis;
    }

    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     * constructor for patient
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * @param diagnosis
     * sets diagnosis
     */
    @Override
    public void setDiagnose(String diagnosis) {

    }

    /**
     * @return
     * returns string with patient and patients diagnosis
     */
    @Override
    public String toString() {
        return "Patient has got this diagnosis{"  + diagnosis + '\'' +
                '}';
    }
}
