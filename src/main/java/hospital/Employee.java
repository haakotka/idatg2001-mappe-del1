package hospital;

public class Employee extends Person {

    /**
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     * constructor for employee
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }


    /**
     * @return
     * returns details about employee. method inherited from Person class
     */
    @Override
    public String toString() {
        return super.toString();
    }
}
