package hospital;

import hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

public class Department {
    private String departmentName;
    private final ArrayList<Employee> employeeArrayList;
    private final ArrayList<Patient> patientArrayList;


    /**
     * @param departmentName
     * Constructor for department. Generates a new department
     */
    public Department(String departmentName) {
        if (!departmentName.isEmpty()){
        this.departmentName = departmentName;
        employeeArrayList = new ArrayList<>();
        patientArrayList = new ArrayList<>();
        }
        else {
            throw new IllegalArgumentException("Can not create department without name");
        }
    }


    /**
     * @return
     * Getter for departmentName
     */
    public String getDepartmentName() {
        return departmentName;
    }


    /**
     * @param departmentName
     * Sets departmentName
     */
    public void setDepartmentName(String departmentName) {
        if (!departmentName.isEmpty()){
        this.departmentName = departmentName;}
        else {
            throw new IllegalArgumentException("Department name can not be empty");
        }
    }


    /**
     * @return
     * Getter for employees
     */
    public ArrayList<Employee> getEmployees() {
        return employeeArrayList;
    }


    /**
     * @param employee
     * Adds employees to employeeArrayList
     */
    public void addEmployee(Employee employee){
        if (employee==null){
            throw new NullPointerException("Employee can not be null");
       }
        if (!employeeArrayList.contains(employee)){
            employeeArrayList.add(employee);
        }
        else {
            System.out.println("Employee already exists in the register");
        }
    }


    /**
     * @return
     * Getter for patients
     */
    public ArrayList<Patient> getPatients(){
        return patientArrayList;
    }


    /**
     * @param patient
     * Adds patients to patientArrayList
     */
    public void addPatient(Patient patient){
        if (patient==null){
            throw new NullPointerException("Patient can not be null");
        }
        if (!patientArrayList.contains(patient)){
            patientArrayList.add(patient);
        }
        else {
            System.out.println("Patient already exists in the register");
        }
    }

    /**
     * @param person
     * @throws RemoveException
     * Method to remove a Person object from the department.
     * Throws RemoveException if unexpected event occurs
     */

    public void remove(Person person) throws RemoveException {
        if (person instanceof Patient){
            if (patientArrayList.contains(person)){
                patientArrayList.remove(person);}
            else {
                throw new RemoveException("Failed to remove patient");
            }
        }
        else if(person instanceof Employee){
            if (employeeArrayList.contains(person)){
                employeeArrayList.remove(person);}
            else {
                throw new RemoveException("Failed to remove employee");
            }
        }
        else{
            throw new RemoveException("Failed to remove person, was not found in patient or employee register");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    /**
     * @return
     * Returns details about the department
     */
    @Override
    public String toString() {
        return "Department{" +
                "departmentName='" + departmentName + '\'' +
                ", employees=" + employeeArrayList +
                ", patients =" + patientArrayList +
                '}';
    }
}
