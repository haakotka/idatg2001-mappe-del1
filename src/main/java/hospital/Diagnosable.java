package hospital;

public interface Diagnosable {

    /**
     * @param diagnosis
     * sets diagnosis
     */
    void setDiagnose(String diagnosis);
}
