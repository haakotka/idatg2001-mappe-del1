package hospital;

import java.util.HashMap;

public class Hospital {
    private final String hospitalName;
    private final HashMap<String,Department> departmentHashMap;

    /**
     * @param hospitalName
     * constructor for hospital. Creates new hospital
     */
    public Hospital(String hospitalName) {
        if (!hospitalName.isEmpty()){
        this.hospitalName = hospitalName;
        departmentHashMap= new HashMap<>();  }
        else throw new IllegalArgumentException("Hospital name can not be empty");
    }


    /**
     * @return
     * getter for hospitalName
     */
    public String getHospitalName() {
        return hospitalName;
    }


    /**
     * @return
     * getter for departments
     */
    public HashMap<String, Department> getDepartments(){
        return departmentHashMap;
    }


    /**
     * @param department
     * adds department to hashmap
     */
    public void addDepartment(Department department){
        //checks if department is not null
        if (department==null){
            throw new NullPointerException("Department can not be null");
        }
        //checks if department already exists and prints out an explanation for the user if it does
        if (!departmentHashMap.containsValue(department)){
        departmentHashMap.put(department.getDepartmentName(), department);
        }
        else System.out.println("Department already exists");
    }


    /**
     * @return
     * prints out info about the hospital
     */
    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                '}';
    }

}
