package hospital;
import hospital.exception.RemoveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class HospitalClientTest {

    HospitalClient hospitalClientTest = new HospitalClient(new Hospital("Test sykehus"));

    int employeesSize;
    int patientsSize;
    //creating employee objects
    Employee employee = new Employee("1","1","1");
    Patient patient  = new Patient("Test","Test","11");




    //filling tests with test data
    @BeforeEach
    void setUp() {
        HospitalTestData.fillRegisterWithTestData(hospitalClientTest.hospital);
        employee = hospitalClientTest.hospital.getDepartments().get("Akutten").getEmployees().get(0);
        employeesSize = hospitalClientTest.hospital.getDepartments().get("Akutten").getEmployees().size();
        patientsSize = hospitalClientTest.hospital.getDepartments().get("Akutten").getPatients().size();
    }

    //checks if remove method works as intended
    @Test
    void clientPositiveRemoveTest(){
        assertEquals(7,employeesSize);

        try{
            hospitalClientTest.hospital.getDepartments().get("Akutten").remove(employee);

        }catch (RemoveException re){
            System.out.println(re.getMessage());
        }
        employeesSize = hospitalClientTest.hospital.getDepartments().get("Akutten").getEmployees().size();
        assertEquals(6,employeesSize);

    }
    //checks if method catches RemoveException on unexpected event
    @Test
    void clientNegativeRemoveTest(){
        assertEquals(2, patientsSize);
        try {
            hospitalClientTest.hospital.getDepartments().get("Akutten").remove(patient);
        } catch (RemoveException re){
            re.printStackTrace();
        }
        patientsSize = hospitalClientTest.hospital.getDepartments().get("Akutten").getPatients().size();
        assertNotEquals(1, patientsSize);
    }



}