package hospital;
import hospital.exception.RemoveException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {

    //creating department, employee and patient object(employees)
    Department department = new Department("Test");
    Employee employee = new Employee("Kåre","Karlsen","13");
    Employee employee2 = new Employee("Kari","Karlsen","14");
    Patient patient = new Patient("Karl","Kul","11");
    Patient patient2 = new Patient("Karen","Kul","11");

    //filling departments with test employees and patients
    @BeforeEach
    void beforeTests() {
        department.getEmployees().add(employee);
        department.getEmployees().add(employee2);
        department.getPatients().add(patient);
    }

    //checks if remove method actually removes an employee
    @Test
    void positiveRemoveTest() {
        assertEquals(2,department.getEmployees().size());
        try {
            department.remove(employee);
            department.remove(employee2);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
        assertEquals(0,department.getEmployees().size());
    }

    //checks that the method throws a removeException if unexpected event happens
    @Test
    void negativeRemoveTest()  {
        assertEquals(1,department.getPatients().size());
        try {
            department.remove(patient2);
        } catch (RemoveException e) {
            e.printStackTrace();
            fail("Failed by intention");

        }
    }
}